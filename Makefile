VERSION=0.9.0

# Speed-up make
MAKEFLAGS += --no-builtin-rules

# Disable all output except errors/warnings
Q?=@

PKG_CONFIG=$(shell which pkg-config)

ifeq (${PKG_CONFIG},)
$(error You need pkg-config installed)
endif

JACK_LDFLAGS=$(shell ${PKG_CONFIG} --libs jack)

# Configure the compiler and linker
CFLAGS=-O3 -Werror -Wall -pedantic -std=c99
LDFLAGS=-flto -lpthread -lm ${JACK_LDFLAGS}

PREFIX?=${HOME}

ifeq (${JACK_LDFLAGS},)
$(error You need libjack and SDK installed)
endif

ifeq ("${PREFIX}","./")
BINPREFIX=${PREFIX}
CFGPREFIX=${PREFIX}
CFGNAME=asciibridge.conf
DEPLOY=2
else
BINPREFIX=${PREFIX}/bin/
CFGPREFIX=${HOME}/
CFGNAME=.asciibridgerc
DEPLOY=1
endif

# Speed-up make
.SUFFIXES:
.SECONDARY:

DEPS=asciibridge.c config.h Makefile
FULL_DEPS=${DEPS} config_head.sh config_foot.sh build.sh
DEPLOY_DEPS=${FULL_DEPS} asciibridge-${VERSION}.gz README.rst examples/aio_studio.h examples/asciibridge.png

all: asciibridge.exe

deploy: asciibridge-${VERSION}.tar.gz.sha512 asciibridge-${VERSION}.gz.sha512
	${Q}echo "" && \
	echo "DONT FORGET:" && \
	echo "" && \
	echo " 1. create a ${VERSION} tag in git and push" && \
	echo " 2. upload the asciibridge-${VERSION}.tar.gz and asciibridge-${VERSION}.gz" && \
	echo "    to a GitLab release based on that ${VERSION} tag" && \
	echo ""

help: README.md
	${Q}cat README.md

asciibridge.exe: ${DEPS}
	${Q}${CC} ${CFLAGS} -o $@ $< ${LDFLAGS} && strip $@

asciibridge: ${FULL_DEPS}
	${Q}echo "#if 0" > $@ && \
	echo "BINPREFIX=${BINPREFIX}" >> $@ && \
	echo "CFGPREFIX=${CFGPREFIX}" >> $@ && \
	echo "CFGNAME=${CFGNAME}" >> $@ && \
	echo "INCDIR=${CFGPREFIX}" >> $@ && \
	cat config_head.sh >> $@ && \
	cat config.h >> $@ && \
	cat config_foot.sh >> $@ && \
	cat build.sh >> $@ && \
	echo "#endif" >> $@ && \
	echo "#define DEPLOY ${DEPLOY}" >> $@ && \
	cat $< >> $@ && \
	chmod +x $@

asciibridge-${VERSION}.gz: asciibridge
	${Q}test -f $@ && (echo "ERROR: File $@ already exist" >&2; exit 1); \
	cat $< | gzip - > $@

asciibridge-${VERSION}.gz.sha512: asciibridge-${VERSION}.gz
	${Q}sha512 $< > $@

asciibridge-${VERSION}.tar.gz: ${DEPLOY_DEPS}
	${Q}mkdir asciibridge-${VERSION} && \
	cp asciibridge.c asciibridge-${VERSION}/. && \
	cp Makefile asciibridge-${VERSION}/. && \
	cp config.h asciibridge-${VERSION}/. && \
	cp config_head.sh asciibridge-${VERSION}/. && \
	cp config_foot.sh asciibridge-${VERSION}/. && \
	cp build.sh asciibridge-${VERSION}/. && \
	mkdir asciibridge-${VERSION}/examples && \
	cp examples/aio_studio.h asciibridge-${VERSION}/examples/. && \
	cp examples/asciibridge.png asciibridge-${VERSION}/examples/. && \
	tar -czf $@ asciibridge-${VERSION}

asciibridge-${VERSION}.tar.gz.sha512: asciibridge-${VERSION}.tar.gz
	${Q}sha512 $< > $@

install: asciibridge
	mkdir -p ${BINPREFIX}
	install -C $< ${BINPREFIX}$<

clean:
	${Q}${RM} -rf asciibridge .asciibridge asciibridge.exe asciibridge.conf *.gz asciibridge-* *~
