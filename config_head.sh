#
# What is this wizardry?
# ----------------------
# This is a valid shell-script stacked upon a C source code. By doing this the
# source code is self-compiling and executable without any additional build
# system.
#
# This also means that your bin-folder where you installed asciibridge to must
# be writable to you. Also - The filesystem must allow execution of binaries,
# which can be assumed if you installed i there in the first place.
#

#
# Default config - If there is no ~/.asciibridge file, lets put a template
# there. So that "something" works at least.
#

#
# Install a new version of a stock configuration to ~/.asciibridgerc
#
if [ ! -f ${CFGPREFIX}${CFGNAME} ]; then
cat <<'EOF'> ${CFGPREFIX}${CFGNAME}
