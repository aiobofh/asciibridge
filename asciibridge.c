#define _XOPEN_SOURCE 500
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <jack/jack.h>

/*****************************************************************************
 * BEWARE! Thiese constants are used in the "config file" too
 */

/* Montor types */
#define MONO 0
#define LEFT 1
#define RIGHT 2

#define MONITOR_WIDTH 5

#define CONFIGURATION                                   \
  typedef struct {                                      \
    char* const name;                                   \
    struct {                                            \
      unsigned int heading_len;                         \
      char* const heading;                              \
      unsigned int monitor_cnt;                         \
      unsigned int spacing_len;                         \
      char* const spacing;                              \
      unsigned int width;                               \
      char* const prefix;                               \
    } const group[GROUP_CNT];                           \
    struct {                                            \
      char const label[MONITOR_WIDTH];                  \
      unsigned int const type;                          \
      char* const port_name;                            \
      char* const source_port_name;                    \
    } const monitor[MONITOR_CNT];                       \
  } config_t;                                           \
                                                        \
  static config_t const config

#define CLIENT(NAME) NAME

#define GROUP(HEADING, MONITOR_CNT, SPACING, PORT_PFX)  \
  { sizeof(HEADING),                                    \
    HEADING,                                            \
    MONITOR_CNT,                                        \
    sizeof(SPACING),                                    \
    SPACING,                                            \
    MONITOR_CNT * MONITOR_WIDTH,                        \
    PORT_PFX}

#define MONITOR(LABEL, TYPE, PORT_NAME, SOURCE_PORT_NAME)      \
  { LABEL,                                                      \
    TYPE,                                                       \
    PORT_NAME,                                                  \
    SOURCE_PORT_NAME }

/*
 * In self-compiling mode the binary should load the user's .asciibridgerc
 * file instad of the development version config.h
 */
#ifdef DEPLOY
# if DEPLOY==1
#  include ".asciibridgerc" /* <---------------------------------- USER INPUT */
# elif DEPLOY==2
#  include "asciibridge.conf"
# else
#  error No supported deployment model specified to pre-processor
# endif
#else
# include "config.h"
#endif

/****************************************************************************/

/*****************************************************************************
 *  __  __       _
 * |  \/  | __ _(_)_ __    _ __  _ __ ___   __ _ _ __ __ _ _ __ ___
 * | |\/| |/ _` | | '_ \  | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \
 * | |  | | (_| | | | | | | |_) | | | (_) | (_| | | | (_| | | | | | |
 * |_|  |_|\__,_|_|_| |_| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|
 *                        |_|              |___/
 */

/* ANSI Colors */
#define COLOR_WIDTH 7
#define WHITE  "\033[0;37m"
#define RED    "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN  "\033[0;32m"

#define RESET_WIDTH 4
#define RESET  "\033[0m"

#define CURSOR_OFF_WIDTH 6
#define CURSOR_OFF "\033[?25l"

#define CURSOR_ON_WIDTH 6
#define CURSOR_ON "\033[?25h"

/* Geometry */
#define BAR_HEIGHT 8

#define M_BAR "  :  "
#define L_BAR "   : "
#define R_BAR " :   "

#define M_TOP "  .  "
#define L_TOP "   . "
#define R_TOP " .   "

#define M_TYP "  M  "
#define L_TYP "   L "
#define R_TYP " R   "

static char padding_fmt[GROUP_CNT][10];
static jack_client_t* client;
static float env[MONITOR_CNT];
static jack_port_t* input_port[MONITOR_CNT];
static pthread_t render_thread;
static int quit = 0;
static inline void cleanup();

static inline void sanity_check() {
  unsigned int group;
  unsigned int monitor;
  unsigned int monitor_idx = 0;

  /* Since M/L/R_BAR/TOP/TYP are 5 and exactly 5 characters wide */
  assert(MONITOR_WIDTH == 5 && "MONITOR WIDTH must be hardcoded to 5");

  for (group = 0; group < GROUP_CNT; group++) {

    /* Format-string for the group headers */
    sprintf(padding_fmt[group], "%%-%ds%%s",
            config.group[group].monitor_cnt * MONITOR_WIDTH);

    /* Sanity check the config */
    for (monitor = 0; monitor < config.group[group].monitor_cnt; monitor++) {
      if (RIGHT == config.monitor[monitor_idx].type) {
        assert(monitor_idx > 0 && "RIGHT channel cannot come first");
        assert(LEFT == config.monitor[monitor_idx - 1].type &&
               "RIGHT must be after LEFT");
      }
      if (LEFT == config.monitor[monitor_idx].type) {
        assert(monitor_idx < MONITOR_CNT && "LEFT channel cannot be last");
        assert(RIGHT == config.monitor[monitor_idx + 1].type &&
               "LEFT must be before RIGHT");
      }
    }
  }
}

static inline float env_read(int port) {
  float tmp = env[port];
  env[port] = 0.0f;
  return tmp;
}

static int peak(jack_nframes_t nframes, void* arg) {
  unsigned int frame_idx;
  unsigned int monitor_idx;

  for (monitor_idx = 0; monitor_idx < MONITOR_CNT; monitor_idx++) {
    jack_default_audio_sample_t *in;
    jack_port_t* in_port = input_port[monitor_idx];
    if (input_port[monitor_idx] == 0) {
      break;
    }

    in = (jack_default_audio_sample_t*)jack_port_get_buffer(in_port, nframes);

    for (frame_idx = 0; frame_idx < nframes; frame_idx++) {
      const float s = fabs(in[frame_idx]);
      if (s > env[monitor_idx]) {
        env[monitor_idx] = s;
      }
    }
  }

  return 0;
}

static inline void jack_init() {
  unsigned int monitor_idx = 0;
  jack_status_t stat;
  unsigned int group;
  unsigned int monitor;

  if (0 == (client = jack_client_open(config.name,
                                      JackNoStartServer,
                                      &stat))) {
    fprintf(stderr, "ERROR: Jack server not running? %d\n", stat);
    exit(EXIT_FAILURE);
  }

  jack_set_process_callback(client, peak, 0);

  if (jack_activate(client)) {
    fprintf(stderr, "ERROR: Could not activate Jack client\n");
    exit(EXIT_FAILURE);
  }

  for (group = 0; group < GROUP_CNT; group++) {
    for (monitor = 0; monitor < config.group[group].monitor_cnt; monitor++) {
      const char **connections;
      char *source_port_name = config.monitor[monitor_idx].source_port_name;
      char name[255];
      jack_port_t* source_port;
      int flags;
      int i;

      snprintf(name, 255, "%s_%02d-%s",
               config.group[group].prefix,
               monitor_idx,
               config.monitor[monitor_idx].port_name);

      if (!(input_port[monitor_idx] = jack_port_register(client,
                                                         name,
                                                         JACK_DEFAULT_AUDIO_TYPE,
                                                         JackPortIsInput, 0))){
        fprintf(stderr, "ERROR: Cannot register '%s'\n", name);
        cleanup();
        exit(EXIT_FAILURE);
      }

      source_port = jack_port_by_name(client, source_port_name);

      if (NULL == source_port) {
        fprintf(stderr, "ERROR: Cannot find port '%s'\n", source_port_name);
        cleanup();
        exit(EXIT_FAILURE);
      }

      flags = jack_port_flags(source_port);

      if (flags & JackPortIsInput) {
        connections = jack_port_get_all_connections(client, source_port);
        for (i = 0; connections && connections[i]; i++) {
          if (0 == jack_disconnect(client, connections[i], name)) {
            fprintf(stderr, "ERROR: Cannot connect '%s' to '%s'\n",
                    connections[i], name);
            cleanup();
            exit(EXIT_FAILURE);
          }
          else {
            fprintf(stderr, "ERROR: Cannot disconnect '%s''\n",
                    connections[i]);
          }
        }
        free(connections);
      }
      else if ((flags & JackPortIsOutput) &&
               jack_connect(client,
                            source_port_name,
                            jack_port_name(input_port[monitor_idx]))) {
        fprintf(stderr,
                "ERROR: Cannot connect port '%s' to '%s' (not an input)\n",
                source_port_name,
                name);
        cleanup();
        exit(EXIT_FAILURE);
      }
      monitor_idx++;
    }
  }
}

static inline void clear() {
  static int first = 1;
  if (first) {
    fwrite(CURSOR_OFF, 1, CURSOR_OFF_WIDTH, stdout);
    fwrite("\033[2J", 1, 4, stdout);
    fflush(stdout);
    first = 0;
  }
  fwrite("\033[0;0H", 1, 6, stdout);
}

static inline void group_titles() {
  unsigned int group;

  /* Reset color */
  fwrite(RESET, 1, RESET_WIDTH, stdout);

  for (group = 0; group < GROUP_CNT; group++) {
    printf(padding_fmt[group],
           config.group[group].heading, config.group[group].spacing);
  }
  puts("");
}

static inline int aiec_scale(float db) {
  float def = 0.0f;

  if (db < -70.0f) {
    def = 0.0f;
  } else if (db < -60.0f) {
    def = (db + 70.0f) * 0.25f;
  } else if (db < -50.0f) {
    def = (db + 60.0f) * 0.5f + 2.5f;
  } else if (db < -40.0f) {
    def = (db + 50.0f) * 0.75f + 7.5;
  } else if (db < -30.0f) {
    def = (db + 40.0f) * 1.5f + 15.0f;
  } else if (db < -20.0f) {
    def = (db + 30.0f) * 2.0f + 30.0f;
  } else if (db < 0.0f) {
    def = (db + 20.0f) * 2.5f + 50.0f;
  } else {
    def = 100.0f;
  }

  return (int)(def * 2.0f);
}
static unsigned int level[MONITOR_CNT];

static inline void meters() {
  unsigned int row;
  unsigned int group;
  unsigned int monitor;

  for (monitor = 0; monitor < MONITOR_CNT; monitor++) {
    const float peak = env_read(monitor);
    static const float bias = 1.0f;
    level[monitor] = aiec_scale(50.0f * log10f(peak * bias));
  }

  for (row = 0; row < BAR_HEIGHT; row++) {
    unsigned int monitor_idx = 0;

    for (group = 0; group < GROUP_CNT; group++) {
      /* Set color for the terminal row */
      fwrite((const char[BAR_HEIGHT][10]){ RED,
                                           YELLOW,
                                           YELLOW,
                                           GREEN,
                                           GREEN,
                                           GREEN,
                                           GREEN,
                                           GREEN }[row],
        1, COLOR_WIDTH, stdout);

      for (monitor = 0; monitor < config.group[group].monitor_cnt; monitor++) {
        const unsigned int row_level = BAR_HEIGHT - row;
        const unsigned int type = config.monitor[monitor_idx].type;
        const unsigned int l1 = level[monitor_idx] / BAR_HEIGHT;
        const unsigned int l2 = l1 >> 1;
        monitor_idx++;
        if (row_level == l2) {
          /*const unsigned int l = level[monitor_idx]; */
          /* Choose characters with correct layout depending on montor type and
             level */
          fwrite((const char[3][2][MONITOR_WIDTH]){{M_BAR,  /* mono */
                                                    M_TOP},
                                                   {L_BAR,  /* left */
                                                    L_TOP},
                                                   {R_BAR,  /* right */
                                                    R_TOP}}[type][1 & l1],
            1, MONITOR_WIDTH, stdout);
        }
        else if (row_level < l2) {
          fwrite((const char[3][MONITOR_WIDTH]){M_BAR,
                                                L_BAR,
                                                R_BAR}[type],
            1, MONITOR_WIDTH, stdout);
        }
        else {
          /* Choose characters with correct layout depending on monitor type
             and level */
          fwrite("     ",
            1, MONITOR_WIDTH, stdout);
        }
      }

      /* Reset color */
      fwrite(RESET, 1, RESET_WIDTH, stdout);

      /* Print spacer between groups */
      fwrite(config.group[group].spacing, 1, config.group[group].spacing_len,
             stdout);
    }
    puts(""); /* Line feed */
  }
}

static inline void types() {
  unsigned int group;
  unsigned int monitor;
  unsigned int monitor_idx = 0;
  for (group = 0; group < GROUP_CNT; group++) {
    for (monitor = 0; monitor < config.group[group].monitor_cnt; monitor++) {
      const unsigned int type = config.monitor[monitor_idx++].type;
      /* Choose channel panning string depending on meter type */
      fwrite((const char[3][MONITOR_WIDTH]){ M_TYP,
                                             L_TYP,
                                             R_TYP }[type],
        1, MONITOR_WIDTH, stdout);
    }

    /* Print spacer between groups */
    fwrite(config.group[group].spacing, 1, config.group[group].spacing_len,
           stdout);
  }
  puts(""); /* Line feed */
}

static inline void labels() {
  unsigned int group;
  unsigned int monitor;
  unsigned int monitor_idx = 0;
  for (group = 0; group < GROUP_CNT; group++) {
    for (monitor = 0; monitor < config.group[group].monitor_cnt; monitor++) {
      fwrite(config.monitor[monitor_idx++].label, 1, MONITOR_WIDTH, stdout);
    }
    /* Print spacer between groups */
    fwrite(config.group[group].spacing, 1, config.group[group].spacing_len,
           stdout);
  }
}

static void* render(void* arg) {

  while (0 == quit) {
    clear();
    group_titles();
    meters();
    types();
    labels();
    fflush(stdout);
    usleep(8 * 10000);
  }

  return 0;
}

static inline void cleanup() {
  unsigned int monitor_idx;

  fwrite(CURSOR_ON, 1, CURSOR_ON_WIDTH, stdout);

  for (monitor_idx = 0; monitor_idx < MONITOR_CNT; monitor_idx++) {
    const jack_port_t* in_port = input_port[monitor_idx];
    const char* in_port_name = jack_port_name(in_port);
    const char **source_name;
    unsigned int source_idx;

    if (NULL == in_port) continue;

    source_name = jack_port_get_all_connections(client, in_port);

    for (source_idx = 0; source_name && source_name[source_idx]; source_idx++) {
      const char* out_port_name = source_name[source_idx];
      jack_disconnect(client, out_port_name, in_port_name);
    }

  }
  jack_client_close(client);
  quit = 1;
}

static void signal_handler(int signal) {
  pthread_cancel(render_thread);
  puts(""); /* Just to give the prompt on a new line, a bit nicer */
  cleanup();
  exit(EXIT_SUCCESS);
}

int main(int argc, char* argv[]) {
  int err;
  sanity_check();
  jack_init();

  if (0 != (err = pthread_create(&render_thread, NULL, &render, NULL))) {
    fprintf(stderr, "ERROR: Unable to create thread: [%s]\n", strerror(err));
    cleanup();
    exit(EXIT_FAILURE);
  }

  signal(SIGINT, signal_handler);

  while (0 == quit) {
    sleep(1);
  }

  exit(EXIT_SUCCESS);
  cleanup();
  return 0;
}
