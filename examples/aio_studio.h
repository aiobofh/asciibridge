/*
 * This is a template configuration for asciibridge
 *
 * Feel free to modify it to suit your own system.
 *
 * NOTE: Config is actually C programming code, and is compiled into the
 *       executable asciibridge program. So handle this with care.
 *
 * HINT: You always need to #define GROUP_CNT and MONITOR_CNT and obviously
 *       the CONFIGURATION assignment. It must contain a CLIENT directive,
 *       giving asciibridge a client name for the audio server. You also are
 *       required to have AT LEAST one GROUP defined. And the monitor count
 *       must summarize up to all the groups monitor count.
 *
 * Groups
 * ------
 *
 * A "group" is a number of monitor meters that are related. For example you
 * could define an "Inputs" group for all your incoming audio signals. Then
 * the number of monitors that are to be located in this group. These are
 * selected from the top of the list of monitors.
 *
 * Synopsis: GROUP([LABEL], [MONITOR COUNT], [SEPARATOR STRING], [PORT PREFIX])
 *
 * - The LABEL is printed on top of the meter-bridge (left aligned)
 * - The MONITOR COUNT is the number of monitors of the MONITOR list that is
 *   to be part of this group
 * - The SEPARATOR STRING is the characters to pad each group with, it could
 *   be just a longer string of spaces ("   ") to add a gap between groups, or
 *   it could be a shorter string with a separator character (" | ") to make
 *   a very strong visual separator impression
 * - The PORT PREFIX is the first part of the Jac/PipeWire port name, it will
 *   be concatenated with as suffix specified on each MONITOR item.
 *
 * Monitor
 * -------
 *
 * A monitor is a single VU-meter that is connected to an audio source. For
 * example "system:capture_1" should normally be your first input channel of
 * your sound card. Its also assigned a type (for layout/graphical reasons)
 * that can be either MONO, LEFT or RIGHT.
 *
 * Synopsis: MONITOR([LABEL], [TYPE], [PORT SUFFIX], [PORT TO MONITOR])
 *
 * - The LABEL is printed under the monitor meter (you can make a longer label
 *   for stereo channels by splitting a longer string between two monitors).
 *   You will get compiler warnings if you make these the wring length :)
 * - The TYPE can be either MONO, LEFT or RIGHT and will be visually indicated
 *   by putting "stereo pairs" a bit closer together. Each meter will also
 *   have a "M" for MONO, "L" for LEFT or "R" for RIGHT between the VU-meter
 *   bar and the LABEL.
 * - The PORT SUFFIX is the suffix added to the Jack/PipeWire port,
 *   concatenated to the PORT PREFIX of the group
 * - The PORT TO MONITOR is the Jack name of the audio source to monitor
 *
 */

/* MANDATORY define. Number of groups (must be at least 1) */
#define GROUP_CNT 5

/* For convenience, declare how many monitors each group should have, remember
   to update the static assignment of the config struct below as well as the
   summarizing macro. */
#define MONITOR_CNT_INPUTS 25
#define MONITOR_CNT_REMOTE 6
#define MONITOR_CNT_SENDS 5
#define MONITOR_CNT_RETURN 4
#define MONITOR_CNT_OUTPUT 2

/* MANDATORY define. Summarize all the monitor groups counters into a total */
#define MONITOR_CNT                   \
  ( MONITOR_CNT_INPUTS +              \
    MONITOR_CNT_REMOTE +              \
    MONITOR_CNT_SENDS +               \
    MONITOR_CNT_RETURN +              \
    MONITOR_CNT_OUTPUT )

/* MANDATORY declaration, must match the GROUP_CNT and MONITOR_CNT */
CONFIGURATION = {
  CLIENT("ASCII-bridge"),
  {
    GROUP("Inputs",  MONITOR_CNT_INPUTS,  " | ", "input"),
    GROUP("Remote",  MONITOR_CNT_REMOTE,  " | ", "remote"),
    GROUP("Sends",   MONITOR_CNT_SENDS,   " | ", "send"),
    GROUP("Returns", MONITOR_CNT_RETURN,  " | ", "return"),
    GROUP("Outputs", MONITOR_CNT_OUTPUT,  "", "output"),
  },
  {
    /* Inputs */
    MONITOR("  C12", LEFT,  "commodore128_sid1",   "system:capture_5"),
    MONITOR("8    ", RIGHT, "commodore128_sid2",   "system:capture_6"),
    MONITOR("micro", LEFT,  "microkorg_xl_left",   "system:capture_9"),
    MONITOR("Korg ", RIGHT, "microkorg_xl_right",  "system:capture_10"),
    MONITOR("Minil", LEFT,  "minilogue_xl_left",   "system:capture_11"),
    MONITOR("ogue ",  RIGHT, "minilogue_xl_right",  "system:capture_12"),
    MONITOR(" Bass", MONO,  "volca_bass",          "system:capture_13"),
    MONITOR(" Moog", MONO,  "moog_sub37",          "system:capture_14"),
    MONITOR("  SR-", LEFT,  "sr18_left",           "system:capture_15"),
    MONITOR("18   ", RIGHT, "sr18_right",          "system:capture_16"),
    MONITOR("Amiga", LEFT,  "amiga1200_left",      "system:capture_17"),
    MONITOR("1200 ", RIGHT, "amiga1200_right",     "system:capture_18"),
    MONITOR(" Amig", LEFT,  "amiga1200_left",      "system:capture_19"),
    MONITOR("a500 ", RIGHT, "amiga1200_right",     "system:capture_20"),
    MONITOR("   C6", LEFT,  "commodore64_sid1",    "system:capture_21"),
    MONITOR("4    ", RIGHT, "commodore64_sid2",    "system:capture_22"),
    MONITOR(" MSX ", MONO,  "msx_fc200",           "system:capture_24"),
    MONITOR(" Bd1 ", MONO,  "drumbrute_kick1",     "system:capture_25"),
    MONITOR(" Bd2 ", MONO,  "drumbrute_kick2",     "system:capture_26"),
    MONITOR(" Snr ", MONO,  "drumbrute_snare",     "system:capture_27"),
    MONITOR("Clap ", MONO,  "drumbrute_clap",      "system:capture_28"),
    MONITOR("HiHat", MONO,  "drumbrute_hihat",     "system:capture_29"),
    MONITOR(" Mix ", MONO,  "drumbrute_mix",       "system:capture_30"),
    MONITOR("   DS", LEFT,  "ds8_a",               "system:capture_31"),
    MONITOR("-8   ", RIGHT, "ds8_b",               "system:capture_32"),

    /* Remote */
    MONITOR(" Me  ", MONO,  "my_talkback",         "system:capture_1"),
    MONITOR("Them ", MONO,  "them_talkback",       "system:capture_36"),
    MONITOR("   Ou", LEFT,  "me_left",             "system:monitor_1"),
    MONITOR("t    ", RIGHT, "me_right",            "system:monitor_2"),
    MONITOR("    I", LEFT,  "them_left",           "system:capture_34"),
    MONITOR("n    ", RIGHT, "them_right",          "system:capture_35"),

    /* Sends */
    MONITOR("Vocod", MONO,  "microkorg_xl_vocoder","system:monitor_3"),
    MONITOR(" Comp", LEFT,  "compressor_left",     "system:monitor_5"),
    MONITOR("ress ", RIGHT, "compressor_right",    "system:monitor_6"),
    MONITOR("MidiV", LEFT,  "midiverb_iii_left",   "system:monitor_7"),
    MONITOR("erb 3", RIGHT, "midiverb_iii_right",  "system:monitor_8"),

    /* Returns */
    MONITOR(" Comp", LEFT,  "compressor_left",     "system:capture_3"),
    MONITOR("ress ", RIGHT, "compressor_right",    "system:capture_4"),
    MONITOR("MidiV", LEFT,  "midiverb_iii_left",   "system:capture_7"),
    MONITOR("erb 3", RIGHT, "midiverb_iii_right",  "system:capture_8"),

    /* Outputs */
    MONITOR(" Main", LEFT,  "main_mix_left",       "system:monitor_1"),
    MONITOR(" mix ", RIGHT, "main_mix_right",      "system:monitor_2")
  }
};
