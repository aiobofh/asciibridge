#
# Self-compiling binary (or execution)
#
elif [ ${BINPREFIX}asciibridge -ot ${BINPREFIX}.asciibridge ]; then
    if [ ! ${CFGPREFIX}${CFGNAME} -nt ${BINPREFIX}asciibridge ]; then
        ${BINPREFIX}.asciibridge $@
        exit ${?}
    fi
fi
if [[ "$(which gcc)" == "" ]]; then
    echo "ERROR: You need to install 'gcc'" >&2
    exit 1
fi
PKG_CONFIG=`which pkg-config`
if [[ "${PKG_CONFIG}" == "" ]]; then
    echo "ERROR: You need to install 'pkg-config'" >&2
    exit 1
fi
JACK_LDFLAGS=`pkg-config --libs jack`
if [[ "${JACK_LDFLAGS}" == "" ]]; then
    echo "ERROR: You need to install 'jack-dev' or 'pipewire-dev''" >&2
    exit 1
fi
ln -sf `realpath ${BINPREFIX}asciibridge` /tmp/.asciibridge.c
cc -O3 -Werror -Wall -pedantic -std=c99 -I${INCDIR} -o ${BINPREFIX}.asciibridge  /tmp/.asciibridge.c -flto -lpthread -lm ${JACK_LDFLAGS} && (rm /tmp/.asciibridge.c && strip ${BINPREFIX}.asciibridge && ${BINPREFIX}.asciibridge $@)
exit ${?}
