
::

    __  __  __ . . ___ ___ .___  __  __
   (--)'--,(__ | | )-_;;--;||__)(_-,(-_
   -------'----------------------------

asciibridge
===========

asciibridge - ASCII-art Jack meter-bridge for terminal junkies

Description
-----------

This program allows you monitor an unlimited number of inputs or outputs on a
Jack or Pipewire capable Linux machine, using a normal text-mode terminal.

.. image:: examples/asciibridge.png

I (the author) wrote this program to have a light-weight real-time meter-bridge
in the bottom of my screen in my music studio. This would enable me to see the
levels (in some sense) each instrument, effect was inputting and what level
my master mix was outputting. And here is the result.

A word about the "self-compiling C-program" distribution model
--------------------------------------------------------------

By default asciibridge is deployed/installed as a self-compiling C program.
this means that no additional build system is required to compile and run the
asciibridge program. I (the author) am an old Amiga computer nerd, and I (the
author) loved the simplicity of just copying a program to a drive and that's
the "installation". This; in combination with compiling it was a bit cooler.

The reason for this design is that I (the author) am completely and utterly
fed-up with config-file dialects and config-file parsers. The config file for
asciibridge is normally stored in ``$HOME/.asciibridge`` and this is actually
a normal C language header file with a static assign of a configuration struct
instance directly, instead of intermediate data formats and input validation.

To be honest I (the author) do not find the format of ``$HOME/.asciibridgerc``
any worse (or better) than any JSON, YAML, TOML, XML or INI file format so...
It's a no-brainer to skip the bloat and parsers.

By having this design the C compiler is given all the possibilities to optimize
the executable binary as best it can. The other reason is to reduce the code
footprint - Making this a very small, effective and efficient program well
suited for a music production environment.

The user experience will be similar to just running any interpreted script, but
the result i highly optimized from your configuration. But it's good to have
some C programming knowledge to deal with compiler warnings/errors that may
occur due to misconfiguration (programming errors) in ``$HOME/.asciibridgerc``.

NOTE: The binary executable will only be re-compiled if the configuration file
is modified, or if a new version of asciibridge is installed.

JUST REMEMBER WHERE YOU SAW IT FIRST, AND GIVE ME SOME CREDIT! <3 :D

Requirements for deployed version
---------------------------------

- Jack or PipeWire (header files and libraries a.k.a. jack-dev/pipewire-dev)
- pkg-config (used to find libjack compile/linking flags to self-compile)
- gcc (able to compile with -std=c99 - tested with gcc 9 and 12)
- strip (reduce binary size)
- which (used to find tools to self-compile)
- cat (used to create template config in $HOME/.asciibridgerc)
- ln (used to temporary create a symbolic link in /tmp)
- rm (used to remove the temporary symbolic link in /tmp(
- echo (used to print error messages during self-compile)
- exit (used to produce correct exit code on failure or success)
- gunzip (to unpack the program)
- sha512sum (to validate the download authenticity)
- $HOME/bin in the path for convenience

Additinal requirements for developers (when using the Makefile)
---------------------------------------------------------------

- GNUMake
- gzip (only used to create a release/deployment)
- tar (to make source distribution archive at release)
- mkdir (used to create $HOME/bin if it does not exist)
- install (if you want to install from repo)

Download and installing
-----------------------

Most people will just need to download the distributed version of asciibridge
directly. It's just a gzip:ed executable, but all the code is there for you to
read and inspect. It's just the beginning of the file that look a bit odd,
where the program logic for self-compilation is located. The C-program part is
just short of 500 lines of code or something like that.

The only time you will need to download the repo (described later) is if you
want the whole development environment to be able to really do modifications
to asciibridge.

Using a browser
^^^^^^^^^^^^^^^

  1. Go to https://gitlab.com/aiobofh/asciibridge/-/releases and download the
     0.9.0 version. Just the .gz (executable) file not the .tar.gz (all
     source tree). Or just download the latest release directly here: https://www.aio.nu/asciibridge-0.9.0.gz

  2. Open a terminal and ``cd`` into your download-folder.

  3. Verify the SHA512 checksum of the file using
     ``sha512sum asciibridge-0.9.0.gz`` which should match::

       1bb933b72db2a004cfecd2c45c1279c74ec3d57d3e34de704d89cad7e14223b4afeb56ab45a9d6f756e68e4bc4e9796f0a0d57295895bb07084efdb7a8d1f305  asciibridge-0.9.0.gz

  4. Unpack the file ``gunzip asciibridge-0.9.0.gz``. This will
     put a file called ``asciibridge`` in the current folder. This is the
     program.

  5. Inspect or modify the source code as you please, the author do not take
     any responsibility for damages to your system.

  6. Make sure you have a ``$HOME/bin`` folder ``makedir -p $HOME/bin``

  7. Move the program to your ``$HOME/bin folder`` by using the ``mv`` command
     ``mv asciibridge $HOME/bin/.``

  8. Run the program a first time to create ``$HOME/.asciibridgerc`` if it
     does not already exist. If you have not updated your environment so that
     you have $HOME/bin in there you will need to execute it using
     ``$HOME/bin/asciibridge`` otherwise just ``asciibridge`` will be fine.
     When the ``$HOME/.asciibridgerc`` is created the program will self-compile
     and run. Then you will (hopefully) now see asciibridge for the first time
     with two input monitors and two output monitors. To quit the program press
     Ctrl+C, this will do a clean exit.

  9. Edit the ``$HOME/.asciibridgerc`` file to suit your needs. Each time you
     make changes to this file asciibridge will be re-compiled and optimized
     for your changes.

Using commandline only
^^^^^^^^^^^^^^^^^^^^^^

This is just copy-paste stuff::

  $ curl https://www.aio.nu/asciibridge-0.9.0.gz
  $ curl https://www.aio.nu/asciibridge-0.9.0.gz.sha512

Verify the package::

  $ sha512sum -c asciibridge-0.9.0.gz.sum

Make sure this reads OK! If you want to be extra sure, verify against this::

  1bb933b72db2a004cfecd2c45c1279c74ec3d57d3e34de704d89cad7e14223b4afeb56ab45a9d6f756e68e4bc4e9796f0a0d57295895bb07084efdb7a8d1f305  asciibridge-0.9.0.gz

Then install it::

  $ gunzip asciibridge-0.9.0.gz
  $ less asciibridge              (Yes. PLEASE read the source)
  $ mkdir -p $HOME/bin
  $ mv asciibridge $HOME/bin/.
  $ $HOME/bin/asciibridge         (Quit with Ctrl+C)

Then edit the ``$HOME/.asciibridgerc`` configuration file and re-run the
program to try out your new config.

Configuration
-------------

asciibridge is configured using a file in the users home-folder called
``$HOME/.asciibridgerc`` in the normal use-case.

NOTE: Config is actually C programming code, and is compiled into the
executable asciibridge program. So handle this with care.

HINT: You always need to #define ``GROUP_CNT`` and ``MONITOR_CNT`` and
obviously the ``CONFIGURATION`` assignment. It must contain a ``CLIENT``
directive, giving asciibridge a client name for the audio server. You are also
required to have AT LEAST one ``GROUP`` and one ``MONITOR`` defined. And the
monitor count must summarize up to all the groups monitor count.

Client
^^^^^^

The program must be given a reasonable name so that Jack or Pipewire audio
servers can relate to it. This is done using the ``CLIENT`` macro. This name
will be found in patch-bays and other tools when interacting with asciibridge.

Synopsis ``CLIENT([A GOOD TEXT STRING])``

Groups
^^^^^^

A "group" is a number of monitor meters that are related. For example you
could define an "Inputs" group for all your incoming audio signals. Then
the number of monitors that are to be located in this group. These are
selected from the top of the list of monitors.

Synopsis: ``GROUP([LABEL],[MONITOR COUNT],[SEPARATOR STRING],[PORT PREFIX])``

- The ``LABEL`` is printed on top of the meter-bridge (left aligned)
- The ``MONITOR COUNT`` is the number of monitors of the ``MONITOR`` list that
  is to be part of this group
- The ``SEPARATOR STRING`` is the characters to pad each group with, it could
  be just a longer string of spaces (``"   "``) to add a gap between groups, or
  it could be a shorter string with a separator character (``" | "``) to make
  a very strong visual separator impression
- The ``PORT PREFIX`` is the first part of the Jac/PipeWire port name, it will
  be concatenated with as suffix specified on each ``MONITOR`` item.

Monitors
^^^^^^^^

A monitor is a single VU-meter that is connected to an audio source. For
example "system:capture_1" should normally be your first input channel of
your sound card. Its also assigned a type (for layout/graphical reasons)
that can be either ``MONO``, ``LEFT`` or ``RIGHT``.

Synopsis: ``MONITOR([LABEL],[TYPE],[PORT SUFFIX],[PORT TO MONITOR])``

- The ``LABEL`` is printed under the monitor meter (you can make a longer label
  for stereo channels by splitting a longer string between two monitors).
  You will get compiler warnings if you make these the wring length :)
- The ``TYPE`` can be either ``MONO``, ``LEFT`` or ``RIGHT`` and will be
  visually indicated by putting "stereo pairs" a bit closer together. Each
  meter will also have a "M" for mono, "L" for left or "R" for right, between
  the VU-meterbar and the ``LABEL``.
- The ``PORT SUFFIX`` is the suffix added to the Jack/PipeWire port,
  concatenated to the ``PORT PREFIX`` of the group
- The ``PORT TO MONITOR`` is the Jack name of the audio source to monitor. It's
  usually ``system:capture_<SOMETHING>`` or ``system:monitor_<SOMETHING>``

Example configuration
^^^^^^^^^^^^^^^^^^^^^

This is an example configuration::

  /* MANDATORY define. Number of groups (must be at least 1) */
  #define GROUP_CNT 2

  /* For convenience, declare how many monitors each group should have, remember
     to update the static assignment of the config struct below as well as the
     summarizing macro. */
  #define MONITOR_CNT_1 2
  #define MONITOR_CNT_2 2

  /* MANDATORY define. Summarize all the monitor groups counters into a total */
  #define MONITOR_CNT                   \
    ( MONITOR_CNT_1 +                   \
      MONITOR_CNT_2 )

  /* MANDATORY declaration, must match the GROUP_CNT and MONITOR_CNT */
  CONFIGURATION = {
    CLIENT("ASCII-bridge"),
    {
      GROUP("Inputs",  MONITOR_CNT_1, " | ", "input"),
      GROUP("Outputs", MONITOR_CNT_2, "", "output"),
    },
    {
      /* Inputs */
      MONITOR("Micro", LEFT,  "microphone_left",    "system:capture_1"),
      MONITOR("phone", RIGHT, "microphone_right",   "system:capture_2"),

      /* Outputs */
      MONITOR(" Main", LEFT,  "main_mix_left",      "system:monitor_1"),
      MONITOR(" mix ", RIGHT, "main_mix_right",     "system:monitor_2"),
    }
  };


Download from repo (master)
---------------------------

To download execute the following command::

  $ git clone https://gitlab.com/aiobofh/asciibridge.git

Building and installing from repo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are several ways to build and deploy asciibridge. These exist due to the
quirky self-compiling design described above. :)

Simple-build from repo (probably NOT what you want to do)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the simplest build::

  $ cd asciibridge
  $ make asciibridge

This will concatenate the script-part and the C source-code part of asciibridge
and make the "script" executable. It's probably a better idea to read the
"Installing" chapter below.

Installing from repo
^^^^^^^^^^^^^^^^^^^^

By default asciibridge will be installed in ``~/bin``::

  $ cd asciibridge
  $ make install

When you execute the asciibridge file for the first time two things will
happen:

1. The example config file will be written to ~/.asciibridgerc if it does not
   already exist.
2. The _real_ binary executable will be compiled and put in the same folder
   as you installed asciibridge to in the first place. It will be called
   .asciibridge and will from now on be used, until you change the
   configuration in ~/.asciibridgerc or updates asciibridge it-self.

Development-builds from repo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you want to develop your own features in asciibridge it you can build a
binary with an example configuration (config.h) and run from the current
folder by invoking::

  $ cd asciibridge
  $ make asciibridge.exe

Since this is a self-contained executable binary it can be run and debugged.

Development-install from repo with configuration file for integration testing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you still want to install and use your own asciibridgerc file in the
current source folder just invoke the following::

  $ cd asciibridge
  $ make install PREFIX=./

At first execution of ./asciibridge the asciibridge.conf file will be generated
in the current folder.

Making a new release
--------------------

Code away, enjoy yorself, relax, play some nice music while hacking. Do
excessive testing of your features.

Then off for some manual script execution::

  $ make clean
  $ sed -i -e 's/<ESC_PREVIOUS_VERSION>/<ESC_NEW_VERSION>/g' Makefile README.rst
  $ make deploy

NOTE: By ESC_PREVIOUS_VERSION and ESC_NEW_VERSION is ment ``\.`` between the
version number parts.

This will give you two gzip archives. ``asciibridge-0.9.0.gz`` and
``asciibridge-0.9.0.tar.gz`` along with a sha512sum file for each of the gzip
files, called ``asciibridge-0.9.0.gz.sha512`` and
``asciibridge-0.9.0.tar.gz.sha512``.

Update the manual with checksums::

  $ sed -i -e 's/<PREVIOUS SHA512SUM>/<NEW SHA512SUM>/g' README.rst

And prepare the git repository for the release::

  $ git status
  $ git add Makefile README.rst
  $ git commit -m "Prepare for relase v<NEW_VERSION>"
  $ git push
  $ git tag v<VERSION> [-m "optional message"]
  $ git push --tags

Upload the ``gz`` and ``.sha512`` files to the publicly available binary
artifactory (web server) of choice.

Prepare the release in GitLab, adding the link to (at least) the
``asciibridge-0.9.0.gz`` file with the same name as text.

Authors and acknowledgment
--------------------------

This program is developed by AiO Secure Teletronics.

Licence
-------

This program is release under the WTFPL licence::

             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                     Version 2, December 2004

  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

  Everyone is permitted to copy and distribute verbatim or modified
  copies of this license document, and changing it is allowed as long
  as the name is changed.

             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

   0. You just DO WHAT THE FUCK YOU WANT TO.

Thank you, and you're welcome! :)
