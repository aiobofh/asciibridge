/*
 * This is a template configuration for asciibridge
 *
 * Feel free to modify it to suit your own system.
 *
 * NOTE: Config is actually C programming code, and is compiled into the
 *       executable asciibridge program. So handle this with care.
 *
 * HINT: You always need to #define GROUP_CNT and MONITOR_CNT and obviously
 *       the CONFIGURATION assignment. It must contain a CLIENT directive,
 *       giving asciibridge a client name for the audio server. You also are
 *       required to have AT LEAST one GROUP defined. And the monitor count
 *       must summarize up to all the groups monitor count.
 *
 * Groups
 * ------
 *
 * A "group" is a number of monitor meters that are related. For example you
 * could define an "Inputs" group for all your incoming audio signals. Then
 * the number of monitors that are to be located in this group. These are
 * selected from the top of the list of monitors.
 *
 * Synopsis: GROUP([LABEL], [MONITOR COUNT], [SEPARATOR STRING], [PORT PREFIX])
 *
 * - The LABEL is printed on top of the meter-bridge (left aligned)
 * - The MONITOR COUNT is the number of monitors of the MONITOR list that is
 *   to be part of this group
 * - The SEPARATOR STRING is the characters to pad each group with, it could
 *   be just a longer string of spaces ("   ") to add a gap between groups, or
 *   it could be a shorter string with a separator character (" | ") to make
 *   a very strong visual separator impression
 * - The PORT PREFIX is the first part of the Jac/PipeWire port name, it will
 *   be concatenated with as suffix specified on each MONITOR item.
 *
 * Monitor
 * -------
 *
 * A monitor is a single VU-meter that is connected to an audio source. For
 * example "system:capture_1" should normally be your first input channel of
 * your sound card. Its also assigned a type (for layout/graphical reasons)
 * that can be either MONO, LEFT or RIGHT.
 *
 * Synopsis: MONITOR([LABEL], [TYPE], [PORT SUFFIX], [PORT TO MONITOR])
 *
 * - The LABEL is printed under the monitor meter (you can make a longer label
 *   for stereo channels by splitting a longer string between two monitors).
 *   You will get compiler warnings if you make these the wring length :)
 * - The TYPE can be either MONO, LEFT or RIGHT and will be visually indicated
 *   by putting "stereo pairs" a bit closer together. Each meter will also
 *   have a "M" for MONO, "L" for LEFT or "R" for RIGHT between the VU-meter
 *   bar and the LABEL.
 * - The PORT SUFFIX is the suffix added to the Jack/PipeWire port,
 *   concatenated to the PORT PREFIX of the group
 * - The PORT TO MONITOR is the Jack name of the audio source to monitor
 *
 */

/* MANDATORY define. Number of groups (must be at least 1) */
#define GROUP_CNT 2

/* For convenience, declare how many monitors each group should have, remember
   to update the static assignment of the config struct below as well as the
   summarizing macro. */
#define MONITOR_CNT_1 2
#define MONITOR_CNT_2 2

/* MANDATORY define. Summarize all the monitor groups counters into a total */
#define MONITOR_CNT                   \
  ( MONITOR_CNT_1 +                   \
    MONITOR_CNT_2 )

/* MANDATORY declaration, must match the GROUP_CNT and MONITOR_CNT */
CONFIGURATION = {
  CLIENT("ASCII-bridge"),
  {
    GROUP("Inputs",  MONITOR_CNT_1, " | ", "input"),
    GROUP("Outputs", MONITOR_CNT_2, "", "output"),
  },
  {
    /* Inputs */
    MONITOR("Micro", LEFT,  "microphone_left",    "system:capture_1"),
    MONITOR("phone", RIGHT, "microphone_right",   "system:capture_2"),

    /* Outputs */
    MONITOR(" Main", LEFT,  "main_mix_left",      "system:monitor_1"),
    MONITOR(" mix ", RIGHT, "main_mix_right",     "system:monitor_2"),
  }
};
